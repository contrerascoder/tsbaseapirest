import { LogLevels } from '@typegoose/typegoose';
import 'dotenv/config'
import http from 'http'
import connectDB from '../utils/connect-db';
import { PORT } from './../config/vars';

connectDB().then(() => {
    try {
        const app = require('../config/app')
        const server = http.createServer(app);
        server.listen(PORT, () => console.log('Servidor escuchando por el puerto ' + PORT))
    } catch (error) {
        console.log(error.message);
        console.log(error);
    }
}).catch(() => {
    console.log('Hubo un fallo con el inicio del programa');
})