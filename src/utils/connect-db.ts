import mongoose from 'mongoose'
import { URIDB } from '../config/vars'

const mOptions : mongoose.ConnectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

export default async function connectDB() {
    try {
        await mongoose.connect(URIDB, mOptions)
        console.log('Se conecto a la base de datos ' + URIDB);
    } catch (error) {
        console.log('No se puedo conectar a la base de datos: ' + error.message)
    }
}