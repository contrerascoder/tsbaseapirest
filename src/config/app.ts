import express from 'express'
import setAppMiddlewares from './../middlewares/index';
const app = express()

setAppMiddlewares(app)

export default app;