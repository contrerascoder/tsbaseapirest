import express from "express";

export default function responseHandler(req:express.Request, res: express.Response) {
    res.status(res.locals?.status).json(res.locals.data)
}