import express from "express";
import apiRouter from '../routes'
import responseHandler from "./response-handler";

export default function setAppMiddlewares(app: express.Application) {
    app.use(express.json())
    app.use('/api', apiRouter)
    app.use(responseHandler)
}